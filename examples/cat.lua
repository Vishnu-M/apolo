require 'apolo':as_global()

--[[
  This program emulates the behaviour of cat command in linux
  General syntax : lua cat.lua [OPTION] [FILE]...
  lua cat.lua --help -> Displays help message
  ]]--
--this script facilitates basic cat functionalities like
--[[
  cat file1 file2 ...
  cat --help
  cat -n file1 file2.....
  ]]--

local line_num=1
function read_from_file(file, flag)
  if not exists(file) then        --checking whether the file exists or not
      return false
  end
  if flag == true then                     --prints with line numbers if -n is specified
    for line in io.lines(file) do
      print("     "..line_num.."  "..line)
      line_num = line_num + 1
    end
  else
    for line in io.lines(file) do
      print(line)
    end
  end
end

--Main

local num_of_args = #arg
local root_dir = current()                --get current directory

for _, value in ipairs(arg) do
  if value == '-n' then                   --presence of -n in any postion of the command-line argument list is to be checked
    flag = true
  end
end

local opts = parseopts{
  named = {
    help  = {type = "switch"}
  },
  multi_positional = 'files'
}

if opts.help then
  print([[Usage: cat [OPTION]... [FILE]...
  Concatenate FILE(s) to standard output.

  With no FILE, or when FILE is -, read standard input.

    -A, --show-all           equivalent to -vET
    -b, --number-nonblank    number nonempty output lines, overrides -n
    -e                       equivalent to -vE
    -E, --show-ends          display $ at end of each line
    -n, --number             number all output lines
    -s, --squeeze-blank      suppress repeated empty output lines
    -t                       equivalent to -vT
    -T, --show-tabs          display TAB characters as ^I
    -u                       (ignored)
    -v, --show-nonprinting   use ^ and M- notation, except for LFD and TAB
        --help     display this help and exit
        --version  output version information and exit

  Examples:
    cat f - g  Output f's contents, then standard input, then g's contents.
    cat        Copy standard input to standard output.

  GNU coreutils online help: <http://www.gnu.org/software/coreutils/>
  Full documentation at: <http://www.gnu.org/software/coreutils/cat>
  or available locally via: info '(coreutils) cat invocation'
  ]])
  return
end

for _, file in ipairs(assert(opts.files, os.exit(1))) do
  if file == '-n' then
                                      -- pass
  else
    read_from_file(file, flag))
  end
end
